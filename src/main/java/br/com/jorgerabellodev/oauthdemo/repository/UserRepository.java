package br.com.jorgerabellodev.oauthdemo.repository;

import br.com.jorgerabellodev.oauthdemo.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {

  User findByUsername(String username);

}
